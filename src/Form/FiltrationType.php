<?php

namespace App\Form;




use App\Entity\Category;
use App\Entity\Tags;

use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class FiltrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

           $builder
               ->add('category', EntityType::class, [
               'class'=> Category::class,
               'choice_label'=> 'name',
               'multiple'=> false,
               'by_reference' => false,
               'label' => 'Категории'
           ])

            ->add('submit',SubmitType::class,['label' => 'Сортировать']);

    }


    public function getBlockPrefix()
    {
        return null;
    }
}
