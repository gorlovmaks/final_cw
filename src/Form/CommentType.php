<?php

namespace App\Form;




use App\Entity\Category;
use App\Entity\Tags;

use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('comment', TextType::class, ['label' => 'Комментарий'])

            ->add('submit',SubmitType::class,['label' => 'Добавить']);

    }


    public function getBlockPrefix()
    {
        return null;
    }
}
