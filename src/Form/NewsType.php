<?php

namespace App\Form;




use App\Entity\Category;
use App\Entity\Tags;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',TextType::class,[
                'label' => 'Заголовок'
            ])
            ->add('content',TextType::class,[
                'label' => 'Контент'
            ])
            ->add('tags', EntityType::class, [
                'class'=> Tags::class,
                'choice_label'=> 'name',
                'multiple'=> true,
                'by_reference' => false,
                'label' => 'Теги'
            ])
            ->add('category', EntityType::class, [
                'class'=> Category::class,
                'choice_label'=> 'name',
                'multiple'=> false,
                'by_reference' => false,
                'label' => 'Категории'
            ])
            ->add('sumbmit',SubmitType::class,['label'=>'Сохранить']);
    }


    public function getBlockPrefix()
    {
        return null;
    }
}
