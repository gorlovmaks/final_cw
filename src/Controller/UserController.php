<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 29.09.18
 * Time: 12:46
 */

namespace App\Controller;


use App\Entity\Post;
use App\Form\FiltrationType;
use App\Form\NewsType;
use App\Repository\PostRepository;;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class UserController extends Controller
{

    /**
     * @Route("profile",name="app_profile")
     */
   public function profileAction(){
       $user = $this->getUser();

       return $this->render('profile.html.twig',[
          'user' => $user
       ]);
   }
}