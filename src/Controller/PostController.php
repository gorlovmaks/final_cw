<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 29.09.18
 * Time: 12:46
 */

namespace App\Controller;


use App\Entity\Actually;
use App\Entity\Approved;
use App\Entity\Comments;
use App\Entity\Post;
use App\Entity\Quality;
use App\Form\CommentType;
use App\Form\FiltrationType;
use App\Form\NewsType;
use App\Form\TagType;
use App\Repository\CommentRepository;
use App\Repository\PostRepository;;

use Doctrine\Common\Persistence\ObjectManager;
use Egulias\EmailValidator\Warning\Comment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Category;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class PostController extends Controller
{
    /**
     *
     * @Route("/",name="app_posts")
     * @param PostRepository $postRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getPosts(
        PostRepository $postRepository,
        Request $request
    ){

        $category_form = $this->createForm(FiltrationType::class);

        $category_form->handleRequest($request);

            $sort_by_category = $category_form->getData()['category'];

            $posts  = $postRepository->findAll();
            if($category_form->getData()){
             $posts = $postRepository->getSortCategory($sort_by_category->getName());
            }
                $paginator = $this->get('knp_paginator');
                $paginator = $paginator->paginate(
                    $posts, /* query NOT result */
                    $request->query->getInt('page', 1)/*page number*/, 2/*limit per page*/
                );

        return $this->render('list_posts.html.twig',[
            'posts' => $paginator,
            'form' => $category_form->createView(),
        ]);
    }

    /**
     *
     * @Route("/post/link/{id}",name="app_get_post")
     * @param PostRepository $postRepository
     * @param int $id
     * @param Request $request
     * @param ObjectManager $manager
     * @param CommentRepository $commentRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getPost(
        PostRepository $postRepository,
        int $id,
        Request $request,
        ObjectManager $manager,
        CommentRepository $commentRepository
    ){
        $post = $postRepository->find($id);
        $form = $this->createForm( CommentType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() and $form->isValid()){
            $comment = new Comments();
            $comment->setPost($post);
            $comment->setAuthor($this->getUser());
            $comment->setComment($form->getData()['comment']);
            $manager->persist($comment);
            $manager->flush();
            return $this->redirect(
                $request
                    ->headers
                    ->get('referer')
            );
        }

        $comments = $commentRepository->findAll();
        return $this->render('post.html.twig',[
            'post' => $post,
            'form' => $form->createView(),
            'comments' => $comments
        ]);
    }


    /**
     *
     * @Route("/post/comment/{id}",name="app_remove_comment")
     * @param int $id
     * @param ObjectManager $manager
     * @param CommentRepository $commentRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeComment(
        int $id,
        ObjectManager $manager,
        CommentRepository $commentRepository,
        Request $request
    ){


        $comment = $commentRepository->find($id);
        $manager->remove($comment);
        $manager->flush();
        return $this->redirect(
            $request
                ->headers
                ->get('referer')
        );
    }

    /**
     *
     * @Route("/posts/create",name="app_create_post")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createPost(Request $request){
        $post = new Post();
        $form = $this->createForm(NewsType::class,$post);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if($form->isSubmitted() and $form->isValid()){
            $post->setAuthor($user);
            $post->setCreatedDate(new \DateTime());
            $em->persist($post);
            $em->flush();
            $this->redirectToRoute('app_posts');
        }

        return $this->render('createPost.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     *
     * @Route("/posts/{id}/{state}",name="app_add_like")
     * @param int $id
     * @param $state
     * @param PostRepository $postRepository
     * @param ObjectManager $objectManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addLike(
        Request $request,
        int $id,
        $state,
        PostRepository $postRepository,
        ObjectManager $objectManager
    ){
        $post = $postRepository->find($id);

        switch ($state) {
            case 'satisfied':
                $like = new Approved();
                $like->setState(true);
                $like->setLiker($this->getUser());
                $post->addLike($like);
                $objectManager->persist($like);
                break;
            case 'displeased':
                $like = new Approved();
                $like->setLiker($this->getUser());
                $like->setState(false);
                $post->addLike($like);
                $objectManager->persist($like);
                break;
            case 'actual':
                $actually = new Actually();
                $actually->setLiker($this->getUser());
                $actually->setState(true);
                $post->addActually($actually);
                $objectManager->persist($actually);
                break;
            case 'not-relevant':
                $actually = new Actually();
                $actually->setLiker($this->getUser());
                $actually->setState(false);
                $post->addActually($actually);
                $objectManager->persist($actually);
                break;
            case 'qualitatively':
                $quality = new Quality();
                $quality->setLiker($this->getUser());
                $quality->setState(true);
                $post->addQuality($quality);
                $objectManager->persist($quality);
                break;
            case 'poorly':
                $quality = new Quality();
                $quality->setLiker($this->getUser());
                $quality->setState(false);
                $post->addQuality($quality);
                $objectManager->persist($quality);
                break;
            default:
               return null;
        }

        $objectManager->flush();

        return $this->redirect(
            $request
                ->headers
                ->get('referer')
        );
    }

}