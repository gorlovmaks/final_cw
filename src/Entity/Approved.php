<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LikeRepository")
 */
class Approved
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean" , length=1024)
     */
    public $state;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="likes")
     */
    private $liker;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Post", inversedBy="likes")
     */
    private $post;

    /**
     * @return bool
     */
    public function isState(): bool
    {
        return $this->state;
    }

    /**
     * @param bool $state
     * @return Approved
     */
    public function setState(bool $state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @param mixed $liker
     * @return Approved
     */
    public function setLiker($liker)
    {
        $this->liker = $liker;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLiker()
    {
        return $this->liker;
    }

    /**
     * @return mixed
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param mixed $post
     */
    public function setPost($post): void
    {
        $this->post = $post;
    }

}