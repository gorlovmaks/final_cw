<?php


// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post
{

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->likes = new ArrayCollection();
        $this->qualities = new ArrayCollection();
        $this->actually = new  ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=1024)
     */
    private $title;
    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $rating = 0;

    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=1024)
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="posts")
     */
    private $author;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="posts")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comments", mappedBy="post")
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Approved", mappedBy="post")
     */
    private $likes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Quality", mappedBy="post")
     */
    private $qualities;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Actually", mappedBy="post")
     */
    private $actually;

    /**
     *
     * @ORM\Column(type="date")
     */
    private $createdDate;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tags", inversedBy="posts")
     */
    private $tags;

    /**
     *
     * @ORM\Column(type="date",nullable=true)
     */
    private $publicatedDate;



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     *
     * @param User $author
     * @return Post
     */
    public function setAuthor(User $author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $title
     * @return Post
     */
    public function setTitle(string $title): Post
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $content
     * @return Post
     */
    public function setContent(string $content): Post
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param mixed $category
     * @return Post
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $createdDate
     * @return Post
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param mixed $publicatedDate
     * @return Post
     */
    public function setPublicatedDate($publicatedDate)
    {
        $this->publicatedDate = $publicatedDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPublicatedDate()
    {
        return $this->publicatedDate;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    public function addTag(Tags $tags): self
    {
        if (!$this->tags->contains($tags)) {
            $this->tags[] = $tags;
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getComments()
    {
        return $this->comments;
    }

    public function addComment(Comments $comment){
        $this->comments[] = $comment;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getQualities()
    {
        return $this->qualities;
    }

    public function addQuality(Quality $quality){
        $this->qualities[] = $quality;
        return $this;
    }

    public function addActually(Actually $actually){
        $this->actually[] = $actually;
        return $this;
    }

    public function addLike(Approved $like){
        $this->likes[] = $like;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @return ArrayCollection
     */
    public function getActually()
    {
        return $this->actually;
    }

    /**
     * @param mixed $tags
     * @return Post
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @param string $rating
     * @return Post
     */
    public function addRating(string $rating)
    {
        $this->rating =+ $rating;
        return $this;
    }

    /**
     * @return string
     */
    public function getRating(): string
    {
        return $this->rating;
    }

}