<?php


// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ActuallyRepository")
 */
class Actually
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean" , length=1024)
     */
    public $state;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="likes")
     */
    private $liker;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Post", inversedBy="actually")
     */
    private $post;

    /**
     * @param bool $state
     * @return Actually
     */
    public function setState(bool $state): Actually
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return bool
     */
    public function isState(): bool
    {
        return $this->state;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLiker()
    {
        return $this->liker;
    }

    /**
     * @param mixed $liker
     * @return Actually
     */
    public function setLiker($liker)
    {
        $this->liker = $liker;
        return $this;
    }

    /**
     * @param mixed $post
     * @return Actually
     */
    public function setPost($post)
    {
        $this->post = $post;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPost()
    {
        return $this->post;
    }

}