<?php


// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QualityRepository")
 */
class Quality
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean" , length=1024)
     */
    public $state;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="likes")
     */
    private $liker;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Post", inversedBy="qualities")
     */
    private $post;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @param bool $state
     * @return Quality
     */
    public function setState(bool $state): Quality
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return bool
     */
    public function isState(): bool
    {
        return $this->state;
    }

    /**
     * @param mixed $liker
     * @return Quality
     */
    public function setLiker($liker)
    {
        $this->liker = $liker;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLiker()
    {
        return $this->liker;
    }

    /**
     * @param mixed $post
     * @return Quality
     */
    public function setPost($post)
    {
        $this->post = $post;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPost()
    {
        return $this->post;
    }

}