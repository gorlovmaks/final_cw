<?php


// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    public function __construct()
    {
        $this->posts = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->likes = new ArrayCollection();
        $this->qualities = new ArrayCollection();
        $this->actually = new ArrayCollection();
        parent::__construct();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=1024)
     */
    private $fullName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Approved", mappedBy="liker")
     */
    private $likes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Quality", mappedBy="liker")
     */
    private $qualities;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Actually", mappedBy="liker")
     */
    private $actually;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="author")
     */
    private $posts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comments", mappedBy="author")
     */
    private $comments;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @param string $fullName
     * @return User
     */
    public function setFullName(string $fullName): User
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @return ArrayCollection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    public function addPost(Post $post){
        $this->posts[] = $post;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getComments()
    {
        return $this->comments;
    }

    public function addComment(Comments $comment){
        $this->comments[] = $comment;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getLikes()
    {
        return $this->likes;
    }

    public function addLike($like){
        $this->likes = $like;
        return $this;
    }

    /**
     * @return int
     */
    public function getQualities()
    {
        $count = 0;
        foreach ($this->likes as $like){
            if($like->state == 1){
                $count =+ 1;
            }else{
                $count =- 1;
            }

        }
        return $count;
    }

    public function addQuality(Quality $quality){
        $this->qualities[] = $quality;
        return $this;
    }

    public function addActually(Actually $actually){
        $this->actually[] = $actually;
        return $this;
    }

    /**
     * @return int
     */
    public function getActually()
    {
        $count = 0;
        foreach ($this->likes as $like){
            if($like->state == 1){
                $count =+ 1;
            }else{
                $count =- 1;
            }

        }
        return $count;
    }

    public function getSum(){
        return (count($this->posts) + $this->getActually() + $this->getQualities())/3;
    }

}