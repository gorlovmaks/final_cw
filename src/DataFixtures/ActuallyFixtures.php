<?php

namespace App\DataFixtures;

use App\Entity\Actually;
use App\Entity\Category;
use App\Entity\Approved;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ActuallyFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {

        for($i = 0; $i < 3; $i++){
            $Actually = new Actually();
            $Actually
                ->setState(false);
            if($i == 0){
                $Actually
                    ->setLiker($this->getReference('user_one'))
                    ->setPost($this->getReference('post_one'));
            }else if($i  ==  1){
                $Actually
                    ->setLiker($this->getReference('user_one'))
                    ->setPost($this->getReference('post_two'));
            }else if($i  ==  2){
                $Actually
                    ->setLiker($this->getReference('user_one'))
                    ->setPost($this->getReference('post_three'));
            }

            $manager->persist($Actually);
            $manager->flush();
        }

        for($i = 0; $i < 3; $i++){
            $Actually = new Actually();
            $Actually
                ->setState(false);
            if($i == 0){
                $Actually
                    ->setLiker($this->getReference('user_two'))
                    ->setPost($this->getReference('post_one'));
            }else if($i  ==  1){
                $Actually
                    ->setLiker($this->getReference('user_two'))
                    ->setPost($this->getReference('post_two'));
            }else if($i  ==  2){
                $Actually
                    ->setLiker($this->getReference('user_two'))
                    ->setPost($this->getReference('post_three'));
            }

            $manager->persist($Actually);
            $manager->flush();
        }

        for($i = 0; $i < 3; $i++){
            $Actually = new Actually();
            $Actually
                ->setState(false);
            if($i == 0){
                $Actually
                    ->setLiker($this->getReference('admin'))
                    ->setPost($this->getReference('post_one'));
            }else if($i  ==  1){
                $Actually
                    ->setLiker($this->getReference('admin'))
                    ->setPost($this->getReference('post_two'));
            }else if($i  ==  2){
                $Actually
                    ->setLiker($this->getReference('admin'))
                    ->setPost($this->getReference('post_three'));
            }

            $manager->persist($Actually);

        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            PostFixtures::class,
        );
    }
}
