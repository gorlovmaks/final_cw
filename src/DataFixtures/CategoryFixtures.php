<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {
        $category1 = new Category();
        $category1
            ->setName('Рыбы');
        $manager->persist($category1);
        $this->addReference('category_fish', $category1);
        $manager->flush();

        $category2 = new Category();
        $category2
            ->setName('Собаки');
        $manager->persist($category2);
        $this->addReference('category_dog', $category2);
        $manager->flush();

        $category3 = new Category();
        $category3
            ->setName('Преступления');
        $manager->persist($category3);
        $this->addReference('category_crime', $category3);
        $manager->flush();

    }
}
