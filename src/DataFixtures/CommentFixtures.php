<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Comments;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PhpParser\CommentTest;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {
        $comment1 = new Comments();
        $comment1
            ->setComment('Полное безобразие')
            ->setPost($this->getReference('post_one'))
            ->setAuthor($this->getReference('user_two'));
        $manager->persist($comment1);
        $manager->flush();

        $comment2 = new Comments();
        $comment2
            ->setComment('Полное безобразие')
            ->setPost($this->getReference('post_two'))
            ->setAuthor($this->getReference('user_one'));
        $manager->persist($comment1);
        $manager->flush();

        $comment3 = new Comments();
        $comment3
            ->setComment('Полное безобразие')
            ->setPost($this->getReference('post_three'))
            ->setAuthor($this->getReference('user_two'));
        $manager->persist($comment1);
        $manager->flush();

        $comment4 = new Comments();
        $comment4
            ->setComment('Полное безобразие')
            ->setPost($this->getReference('post_one'))
            ->setAuthor($this->getReference('admin'));
        $manager->persist($comment1);
        $manager->flush();

    }

    public function getDependencies()
    {
        return array(
            PostFixtures::class
        );
    }
}
