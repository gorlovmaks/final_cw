<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Approved;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LikeFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {

        for($i = 0; $i < 3; $i++){
            $like = new Approved();
            $like
                ->setState(true);
                if($i == 0){
                    $like
                        ->setLiker($this->getReference('user_one'))
                         ->setPost($this->getReference('post_one'));
                }else if($i  ==  1){
                    $like
                        ->setLiker($this->getReference('user_one'))
                        ->setPost($this->getReference('post_two'));
                 }else if($i  ==  2){
                    $like
                        ->setLiker($this->getReference('user_one'))
                        ->setPost($this->getReference('post_three'));
                }

            $manager->persist($like);
            $manager->flush();
        }

        for($i = 0; $i < 3; $i++){
            $like = new Approved();
            $like
                ->setState(false);
            if($i == 0){
                $like
                    ->setLiker($this->getReference('user_two'))
                    ->setPost($this->getReference('post_one'));
            }else if($i  ==  1){
                $like
                    ->setLiker($this->getReference('user_two'))
                    ->setPost($this->getReference('post_two'));
            }else if($i  ==  2){
                $like
                    ->setLiker($this->getReference('user_two'))
                    ->setPost($this->getReference('post_three'));
            }

            $manager->persist($like);
            $manager->flush();
        }

        for($i = 0; $i < 3; $i++){
            $like = new Approved();
            $like
                ->setState(false);
            if($i == 0){
                $like
                    ->setLiker($this->getReference('admin'))
                    ->setPost($this->getReference('post_one'));
            }else if($i  ==  1){
                $like
                    ->setLiker($this->getReference('admin'))
                    ->setPost($this->getReference('post_two'));
            }else if($i  ==  2){
                $like
                    ->setLiker($this->getReference('admin'))
                    ->setPost($this->getReference('post_three'));
            }

            $manager->persist($like);

        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            PostFixtures::class,
        );
    }
}
