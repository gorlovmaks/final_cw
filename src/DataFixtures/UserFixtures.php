<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    ){

        $admin = new User();
        $admin
            ->setEmail('admin@gmail.com')
            ->setUsername('ivan_administrator')
            ->setPlainPassword('123')
            ->setEnabled(true)
            ->setRoles(['ROLE_ADMIN'])
            ->setFullName('Администратор Иван Иванович');

        $manager->persist($admin);
        $this->addReference('admin', $admin);
        $manager->flush();

        $user1 = new User();
        $user1
            ->setEmail('user1@mail.ru')
            ->setUsername('ivan_polzovatel')
            ->setPlainPassword('123')
            ->setEnabled(true)
            ->setRoles(['ROLE_USER'])
            ->setFullName('Пользователь Иван Иванович');
        $manager->persist($user1);
        $this->addReference('user_one', $user1);
        $manager->flush();

        $user2 = new User();
        $user2
            ->setEmail('user2@mail.ru')
            ->setUsername('serega_polzovatel')
            ->setPlainPassword('123')
            ->setEnabled(true)
            ->setRoles(['ROLE_USER'])
            ->setFullName('Пользователь Сергей Сергеевич');
        $manager->persist($user2);
        $this->addReference('user_two', $user2);
        $manager->flush();
    }
}
