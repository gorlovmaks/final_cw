<?php

namespace App\DataFixtures;


use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PostFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {

        $user1 = $this->getReference('user_one');
        $user2 = $this->getReference('user_two');
        $admin = $this->getReference('admin');

        $category1 = $this->getReference('category_fish');
        $category2 = $this->getReference('category_dog');
        $category3 = $this->getReference('category_crime');

        $tag1 = $this->getReference('tag_one');
        $tag2 = $this->getReference('tag_two');
        $tag3 = $this->getReference('tag_three');
        $tag4 = $this->getReference('tag_four');

        $post1 = new Post();
        $post1
            ->setTitle('Ходячие рыбы в Бишкеке')
            ->setContent('Как сообщают учёные, эксперимент выявил существенные анатом' .
                'ические и поведенческие изменения. Когда выращенные на суше многопёры передвига' .
                'лись пешком, их походка становилась более эффективной и быстрой при условии, чт' .
                'о особи ставили свои плавники максимально близко к телу — в 3,5 раза ближе обыч' .
                'ного, а головы поднимали в полтора раза выше, чем в воде."Что касается анатомич' .
                'еских изменений, то наиболее сильно они проявились у грудном отделе рыб. Скелет' .
                ' стал более удлинённым, а связки в грудной клетке существенно укрепились, очеви' .
                'дно, для того чтобы увеличить поддержку во время ходьбы. Также ослабли связи ту' .
                'ловища с черепом для обеспечения большей манёвренности шейного отдела", — поясн' .
                'яет соавтор исследования Трина Дю (Trina Du) из университета Макгилла.')
            ->setAuthor($user1)
            ->setCategory($category1)
            ->addTag($tag2)
            ->addTag($tag1)
            ->setCreatedDate(new \DateTime())
            ->setPublicatedDate(new \DateTime());
        $manager->persist($post1);
        $this->addReference('post_one', $post1);
        $manager->flush();

        $post2 = new Post();
        $post2
            ->setTitle('Служебные собаки')
            ->setContent('Из десятка ботинок служебные собаки всегда выберут нужный, этому их учат буквально с рождения – считается, что все природные навыки щенка начинают притупляться уже после двух месяцев. Сначала ведется работа по свежему следу – с этой задачей, как правило, все справляются на "ура".
"Очень ошибочное мнение, что идет только по отпечатку обуви. При передвижении человека с него осыпается очень много частичек, молекул, принадлежащих именно конкретному человеку. То есть, его индивидуальный запах", - объясняют кинологи.
Цель найдена – четвероногий служивый получает в награду лакомство. Мотивация – главное в обучении. Для кого-то это сладости, для кого-то – игрушки. Все зависит от собаки и, конечно, кинолога.')
            ->setAuthor($user2)
            ->setCategory($category2)
            ->addTag($tag1)
            ->setCreatedDate(new \DateTime())
            ->setPublicatedDate(new \DateTime());
        $manager->persist($post2);
        $this->addReference('post_two', $post2);
        $manager->flush();

        $post3 = new Post();
        $post3
            ->setTitle('Украли машину в бишкеке и обманывали всех в округ')
            ->setContent('По данным МВД РФ, в 2017 году в России было похищено 33 758 автомобилей. Во' .
                'зглавляет «хит-парад» детище отечественного автопрома - за минувший год в нашей стране был' .
                'о похищено 5756 машин марки Lada. Второе место в рейтинге у Toyota - 3797. Дальше идет кор' .
                'ейский бренд Hyundai: 2056 похищенных машин, основную часть которых составляют народные So' .
                'laris. Также в верхних строчках списка оказался и сестринский бренд KIA и японский Nissan:' .
                ' 1890 и 1197 автомобилей соответственно. ПРОВОКАЦИЯ Автомобилиста выманивают из машины, сы' .
                'митировав неисправность. К трюку с заткнутой выхлопной трубой добавились вариации. Наприме' .
                'р, подложенная под колесо пустая пластиковая бутылка, которая издает жуткий звук. Заподозр' .
                'ивший опасную неисправность, владелец выходит из машины, оставив ключ в замке.')
            ->setAuthor($admin)
            ->setCategory($category3)
            ->addTag($tag3)
            ->addTag($tag4)
            ->setCreatedDate(new \DateTime())
            ->setPublicatedDate(new \DateTime());
        $manager->persist($post3);
        $this->addReference('post_three', $post3);
        $manager->flush();

    }
    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            CategoryFixtures::class,
            TagFixtures::class,
        );
    }
}
