<?php

namespace App\DataFixtures;

use App\Entity\Actually;
use App\Entity\Category;
use App\Entity\Approved;
use App\Entity\Quality;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class QualityFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {

        for($i = 0; $i < 3; $i++){
            $quality = new Quality();
            $quality
                ->setState(true);
            if($i == 0){
                $quality
                    ->setLiker($this->getReference('user_one'))
                    ->setPost($this->getReference('post_one'));
            }else if($i  ==  1){
                $quality
                    ->setLiker($this->getReference('user_one'))
                    ->setPost($this->getReference('post_two'));
            }else if($i  ==  2){
                $quality
                    ->setLiker($this->getReference('user_one'))
                    ->setPost($this->getReference('post_three'));
            }

            $manager->persist($quality);
            $manager->flush();
        }

        for($i = 0; $i < 3; $i++){
            $quality = new Quality();
            $quality
                ->setState(true);
            if($i == 0){
                $quality
                    ->setLiker($this->getReference('user_two'))
                    ->setPost($this->getReference('post_one'));
            }else if($i  ==  1){
                $quality
                    ->setLiker($this->getReference('user_two'))
                    ->setPost($this->getReference('post_two'));
            }else if($i  ==  2){
                $quality
                    ->setLiker($this->getReference('user_two'))
                    ->setPost($this->getReference('post_three'));
            }

            $manager->persist($quality);
            $manager->flush();
        }

        for($i = 0; $i < 3; $i++){
            $quality = new Quality();
            $quality
                ->setState(true);
            if($i == 0){
                $quality
                    ->setLiker($this->getReference('admin'))
                    ->setPost($this->getReference('post_one'));
            }else if($i  ==  1){
                $quality
                    ->setLiker($this->getReference('admin'))
                    ->setPost($this->getReference('post_two'));
            }else if($i  ==  2){
                $quality
                    ->setLiker($this->getReference('admin'))
                    ->setPost($this->getReference('post_three'));
            }

            $manager->persist($quality);

        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            PostFixtures::class,
        );
    }
}
