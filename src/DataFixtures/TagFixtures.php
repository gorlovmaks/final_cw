<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Tags;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TagFixtures extends Fixture
{

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {
        $tag1 = new Tags();
        $tag1
            ->setName('Животное');
        $manager->persist($tag1);
        $this->addReference('tag_one', $tag1);
        $manager->flush();

        $tag2 = new Tags();
        $tag2
            ->setName('Водоплавающие');
        $manager->persist($tag2);
        $this->addReference('tag_two', $tag2);
        $manager->flush();

        $tag3 = new Tags();
        $tag3
            ->setName('Ограбление');
        $manager->persist($tag3);
        $this->addReference('tag_three', $tag3);
        $manager->flush();

        $tag4 = new Tags();
        $tag4
            ->setName('Мошенничество');
        $manager->persist($tag4);
        $this->addReference('tag_four', $tag4);
        $manager->flush();

    }
}
